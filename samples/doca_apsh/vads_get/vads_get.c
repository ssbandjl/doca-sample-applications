/*
 * Copyright (c) 2022 NVIDIA CORPORATION & AFFILIATES, ALL RIGHTS RESERVED.
 *
 * This software product is a proprietary product of NVIDIA CORPORATION &
 * AFFILIATES (the "Company") and all right, title, and interest in and to the
 * software product, including all associated intellectual property rights, are
 * and shall remain exclusively with the Company.
 *
 * This software product is governed by the End User License Agreement
 * provided with the software product.
 *
 */
#include <inttypes.h>

#include <doca_apsh.h>
#include <doca_log.h>

#include "apsh_common.h"

DOCA_LOG_REGISTER(VADS_GET);

int vads_get(const char *dma_device_name, const char *pci_vuid, enum doca_apsh_system_os os_type,
	     DOCA_APSH_PROCESS_PID_TYPE pid)
{
	int res, i;
	struct doca_apsh_ctx *apsh_ctx;
	struct doca_apsh_system *sys;
	struct doca_apsh_process *proc, **processes;
	int num_vads;
	struct doca_apsh_vad **vads_list;
	/* Hardcoded pathes to the files created by doca_apsh_config tool */
	const char *os_symbols = "/tmp/symbols.json";
	const char *mem_region = "/tmp/mem_regions.json";

	/* Init */
	res = init_doca_apsh(&apsh_ctx, dma_device_name);
	if (res) {
		DOCA_LOG_ERR("Failed to init the DOCA APSH lib.");
		return res;
	}
	DOCA_LOG_INFO("DOCA APSH lib context init successful.");

	res = init_doca_apsh_system(apsh_ctx, os_type, os_symbols, mem_region, pci_vuid, &sys);
	if (res) {
		DOCA_LOG_ERR("Failed to init the system context.");
		return res;
	}
	DOCA_LOG_INFO("DOCA APSH system context created.");

	process_get(pid, sys, &processes, &proc);
	if (proc == NULL) {
		DOCA_LOG_ERR("Process pid %d not found. Is it possible host is shutting down or unstable.", pid);
		cleanup_doca_apsh(apsh_ctx, sys);
		return -1;
	}
	DOCA_LOG_INFO("Process with PID %u found.", pid);
	DOCA_LOG_INFO("proc(%d) name: %s", pid, doca_apsh_proc_info_get(proc, DOCA_APSH_PROCESS_COMM));

	res = doca_apsh_vads_get(proc, &vads_list, &num_vads);
	if (res != 0) {
		DOCA_LOG_ERR("Failed to read vads info from host.");
		doca_apsh_processes_free(processes);
		cleanup_doca_apsh(apsh_ctx, sys);
		return res;
	}
	DOCA_LOG_INFO("Successfully performed %s. Host proc(%d) contains %d vads.", __func__, pid, num_vads);

	/* Print some attributes of the vads */
	DOCA_LOG_INFO("First 5 vads for process %u:", pid);
	for (i = 0; i < num_vads && i < 5; ++i) {
		DOCA_LOG_INFO("\tVad %d  -  Process name: %s, start address: 0x%"PRIx64", end address: 0x%"PRIx64, i,
			doca_apsh_vad_info_get(vads_list[i], DOCA_APSH_VMA_PROCESS_NAME),
			doca_apsh_vad_info_get(vads_list[i], DOCA_APSH_VMA_VM_START),
			doca_apsh_vad_info_get(vads_list[i], DOCA_APSH_VMA_VM_END));
	}

	/* Cleanup */
	doca_apsh_vads_free(vads_list);
	doca_apsh_processes_free(processes);
	cleanup_doca_apsh(apsh_ctx, sys);
	return 0;
}
