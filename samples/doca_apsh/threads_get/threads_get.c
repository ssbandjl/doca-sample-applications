/*
 * Copyright (c) 2022 NVIDIA CORPORATION & AFFILIATES, ALL RIGHTS RESERVED.
 *
 * This software product is a proprietary product of NVIDIA CORPORATION &
 * AFFILIATES (the "Company") and all right, title, and interest in and to the
 * software product, including all associated intellectual property rights, are
 * and shall remain exclusively with the Company.
 *
 * This software product is governed by the End User License Agreement
 * provided with the software product.
 *
 */
#include <doca_apsh.h>
#include <doca_log.h>

#include "apsh_common.h"

DOCA_LOG_REGISTER(THREADS_GET);

int threads_get(const char *dma_device_name, const char *pci_vuid, DOCA_APSH_PROCESS_PID_TYPE pid)
{
	int res, i;
	struct doca_apsh_ctx *apsh_ctx;
	struct doca_apsh_system *sys;
	struct doca_apsh_process *proc, **processes;
	int num_threads;
	struct doca_apsh_thread **threads_list;
	/* Hardcoded pathes to the files created by doca_apsh_config tool */
	const char *os_symbols = "/tmp/symbols.json";
	const char *mem_region = "/tmp/mem_regions.json";
	enum doca_apsh_system_os os_type = DOCA_APSH_SYSTEM_LINUX;

	/* Init */
	res = init_doca_apsh(&apsh_ctx, dma_device_name);
	if (res) {
		DOCA_LOG_ERR("Failed to init the DOCA APSH lib.");
		return res;
	}
	DOCA_LOG_INFO("DOCA APSH lib context init successful.");

	res = init_doca_apsh_system(apsh_ctx, os_type, os_symbols, mem_region, pci_vuid, &sys);
	if (res) {
		DOCA_LOG_ERR("Failed to init the system context.");
		return res;
	}
	DOCA_LOG_INFO("DOCA APSH system context created.");

	process_get(pid, sys, &processes, &proc);
	if (proc == NULL) {
		DOCA_LOG_ERR("Process pid %d not found. It is possible host is shutting down or unstable.", pid);
		cleanup_doca_apsh(apsh_ctx, sys);
		return -1;
	}
	DOCA_LOG_INFO("Process with PID %u found.", pid);
	DOCA_LOG_INFO("proc(%d) name: %s", pid, doca_apsh_proc_info_get(proc, DOCA_APSH_PROCESS_COMM));

	res = doca_apsh_threads_get(proc, &threads_list, &num_threads);
	if (res != 0) {
		DOCA_LOG_ERR("Failed to read threads info from host.");
		doca_apsh_processes_free(processes);
		cleanup_doca_apsh(apsh_ctx, sys);
		return res;
	}
	DOCA_LOG_INFO("Successfully performed %s. Host proc(%d) contains %d threads.", __func__, pid, num_threads);

	/* Print some attributes of the threads */
	DOCA_LOG_INFO("Threads for process %u:", pid);
	for (i = 0; i < num_threads; ++i) {
		DOCA_LOG_INFO("\tThread %d  -  TID: %u, Thread Name: %s, Thread state: %ld", i,
			doca_apsh_thread_info_get(threads_list[i], DOCA_APSH_THREAD_TID),
			doca_apsh_thread_info_get(threads_list[i], DOCA_APSH_THREAD_LINUX_THREAD_NAME),
			doca_apsh_thread_info_get(threads_list[i], DOCA_APSH_THREAD_STATE));
	}

	/* Cleanup */
	doca_apsh_threads_free(threads_list);
	doca_apsh_processes_free(processes);
	cleanup_doca_apsh(apsh_ctx, sys);
	return 0;
}
