/*
 * Copyright (c) 2021-2022 NVIDIA CORPORATION & AFFILIATES, ALL RIGHTS RESERVED.
 *
 * This software product is a proprietary product of NVIDIA CORPORATION &
 * AFFILIATES (the "Company") and all right, title, and interest in and to the
 * software product, including all associated intellectual property rights, are
 * and shall remain exclusively with the Company.
 *
 * This software product is governed by the End User License Agreement
 * provided with the software product.
 *
 */

#include <string.h>

#include <doca_argp.h>
#include <utils.h>

#include "secure_channel_core.h"

DOCA_LOG_REGISTER(SECURE_CHANNEL);

int
main(int argc, char **argv)
{
	/* Init application structs with default values */
	struct doca_argp_program_general_config *doca_general_config;
	struct doca_argp_program_type_config type_config = {
		.is_dpdk = false,
		.is_grpc = false,
	};
	struct secure_channel_config app_cfg = {
		.mode = NO_VALID_INPUT,
	};

	/* Define Comm Channel endpoint attributes */
	struct doca_comm_channel_init_attr attr;
	struct doca_comm_channel_ep_t *ep;
	struct doca_comm_channel_addr_t *peer_addr = NULL;
	doca_error_t ret;

	/* Parse cmdline/json arguments */
	doca_argp_init("secure_channel", &type_config, &app_cfg);
	register_secure_channel_params();
	doca_argp_start(argc, argv, &doca_general_config);

	/* Set queue-pair attributes */
	configure_ep_qp_attributes(&attr);

	/* Create Secure Channel endpoint */
	ret = create_secure_channel_ep(&ep, &attr);
	if (ret != DOCA_SUCCESS)
		return ret;

	/* Start client/server logic */
	if (app_cfg.mode == CLIENT)
		ret = secure_channel_client(ep, &peer_addr, &app_cfg);
	else
		ret = secure_channel_server(ep, &peer_addr);

	if (ret != DOCA_SUCCESS) {
		doca_comm_channel_ep_destroy(ep);
		return ret;
	}

	/* Disconnect from current peer connection */
	ret = doca_comm_channel_ep_disconnect(ep, peer_addr);
	if (ret != DOCA_SUCCESS) {
		DOCA_LOG_ERR("Failed to disconnect channel: %s", doca_get_error_string(ret));
		doca_comm_channel_ep_destroy(ep);
		return ret;
	}

	/* Destroy endpoint */
	ret = doca_comm_channel_ep_destroy(ep);
	if (ret != DOCA_SUCCESS)
		DOCA_LOG_ERR("Failed to destroy Comm Channel endpoint: %s", doca_get_error_string(ret));

	/* ARGP cleanup */
	doca_argp_destroy();

	return ret;
}
