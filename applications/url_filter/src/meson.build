#
# Copyright (c) 2021-2022 NVIDIA CORPORATION & AFFILIATES, ALL RIGHTS RESERVED.
#
# This software product is a proprietary product of NVIDIA CORPORATION &
# AFFILIATES (the "Company") and all right, title, and interest in and to the
# software product, including all associated intellectual property rights, are
# and shall remain exclusively with the Company.
#
# This software product is governed by the End User License Agreement
# provided with the software product.
#

app_dependencies += dependency('doca-argp')
app_dependencies += dependency('doca-dpi')
app_dependencies += dependency('doca-common')
app_dependencies += dependency('doca-telemetry')

app_srcs += files([
	'url_filter_core.c',
	common_dir_path + '/dpdk_utils.c',
	common_dir_path + '/dpi_worker.c',
	common_dir_path + '/offload_rules.c',
	common_dir_path + '/utils.c',
])

vanilla_app_srcs = [
	APP_NAME + '.c',
]

executable(DOCA_PREFIX + APP_NAME,
	app_srcs + vanilla_app_srcs,
	dependencies : app_dependencies,
	include_directories : app_inc_dirs,
	install: false)

# Only relevant if gRPC is supported
if get_option('enable_grpc_support')
	subdir('grpc')
endif
