/*
 * Copyright (c) 2021-2022 NVIDIA CORPORATION & AFFILIATES, ALL RIGHTS RESERVED.
 *
 * This software product is a proprietary product of NVIDIA CORPORATION &
 * AFFILIATES (the "Company") and all right, title, and interest in and to the
 * software product, including all associated intellectual property rights, are
 * and shall remain exclusively with the Company.
 *
 * This software product is governed by the End User License Agreement
 * provided with the software product.
 *
 */
#include <string.h>
#include <errno.h>
#include <inttypes.h>

#include <doca_argp.h>
#include <doca_log.h>
#include <doca_regex.h>
#include <doca_regex_mempool.h>

#include <utils.h>

#include "file_scan_core.h"

DOCA_LOG_REGISTER(FILE_SCAN);

static int
get_line_number(char *data, uint32_t offset, int *last_newline_idx)
{
	int res = 1, idx;

	*last_newline_idx = 0;
	for (idx = 0; idx < offset; idx++) {
		if (data[idx] == '\n') {
			*last_newline_idx = idx;
			res++;
		}
	}
	return res;
}

static void
report_results(struct file_scan_config *app_cfg, struct doca_regex_job_response *job_resp,
	       struct doca_regex_mempool *matches_mp)
{
	int regex_match_line_nb, last_newline_idx, regex_match_i = 0;
	struct doca_regex_match *match;
	int match_index, match_start_offset;

	if (job_resp->detected_matches > 0)
		DOCA_LOG_INFO("Job %" PRIu64 " complete. Detected %d match(es)", job_resp->id,
				job_resp->detected_matches);
	if (job_resp->num_matches == 0)
		return;
	app_cfg->total_matches += job_resp->detected_matches;
	/* Match start is relative the whole file and not the chunk */
	match_start_offset = job_resp->id * app_cfg->chunk_size;
	for (match = job_resp->matches; match != NULL;) {

		regex_match_line_nb = get_line_number(app_cfg->data_buffer, match_start_offset + match->match_start,
						      &last_newline_idx);
		match_index = match_start_offset + (match->match_start - last_newline_idx);
		if (app_cfg->csv_fp != NULL)
			fprintf(app_cfg->csv_fp, "%d,%d,%d,%d\n", regex_match_line_nb, match_index,
									match->length, match->rule_id);
		DOCA_LOG_INFO("Match %d:", regex_match_i++);
		DOCA_LOG_INFO("\t\tLine Number:  %12d", regex_match_line_nb);
		DOCA_LOG_INFO("\t\tMatch Index:  %12d", match_index);
		DOCA_LOG_INFO("\t\tMatch Length: %12d", match->length);
		DOCA_LOG_INFO("\t\tRule Id:      %12d", match->rule_id);

		struct doca_regex_match *const to_release_match = match;

		match = match->next;
		doca_regex_mempool_obj_put(matches_mp, to_release_match);
	}

	job_resp->matches = NULL;
}

/* Initialize the first job request.
 * returns the total number of bytes to be scanned.
 */
static long
init_job_request(struct file_scan_config *app_cfg, struct doca_regex_job_request *job_request)
{
	long total_length = (long)app_cfg->file_buffer.length;

	job_request->id = 0;
	job_request->buffer = &app_cfg->file_buffer;
	job_request->rule_group_ids[0] = 1; /* Set the rules group id to use. */
	/* Set the length according to max chunk size. */
	if (app_cfg->chunk_size > 0 && job_request->buffer->length > app_cfg->chunk_size)
		app_cfg->file_buffer.length = app_cfg->chunk_size;

	return total_length;
}

static uint32_t
file_scan_enqueue_job(struct file_scan_config *app_cfg, long *remaining_bytes)
{
	int enq_result;
	uint32_t nb_enqueued = 0;

	while (*remaining_bytes != 0) {
		enq_result = doca_regex_enqueue(app_cfg->doca_regex, REGEX_QP_INDEX, &app_cfg->job_request, true);
		if (enq_result == 0)
			break; /* QP is full, try to dequeue. */
		if (enq_result < 0)
			APP_EXIT("Unable to enqueue job. [%s]", strerror(abs(enq_result)));
		*remaining_bytes -= app_cfg->file_buffer.length; /* Update remaining bytes to scan. */
		nb_enqueued++;

		/* Prepare next chunk. */
		app_cfg->job_request.id++;
		app_cfg->file_buffer.address += app_cfg->file_buffer.length; /* Update address to next chunk. */
		/* Chunk length can't be greater than the remaining bytes. */
		if (app_cfg->file_buffer.length > *remaining_bytes)
			app_cfg->file_buffer.length = *remaining_bytes;
	}

	return nb_enqueued;
}

static uint32_t
file_scan_dequeue_job(struct file_scan_config *app_cfg)
{
	int deq_result;
	int dequeues_idx;
	uint32_t nb_dequeued = 0;
	struct doca_regex_job_response job_responses[JOB_RESPONSE_SIZE];

	do {
		deq_result = doca_regex_dequeue(app_cfg->doca_regex, REGEX_QP_INDEX, job_responses, JOB_RESPONSE_SIZE);
		if (deq_result < 0)
			APP_EXIT("Unable to dequeue results. [%s]", strerror(abs(deq_result)));

		nb_dequeued += (uint32_t)deq_result;
		for (dequeues_idx = 0; dequeues_idx < deq_result; dequeues_idx++)
			report_results(app_cfg, job_responses + dequeues_idx, app_cfg->matches_mp);
	} while (deq_result != 0);

	return nb_dequeued;
}

int
main(int argc, char **argv)
{
	struct file_scan_config app_cfg = {0};
	struct doca_argp_program_general_config *doca_general_config;
	struct doca_argp_program_type_config type_config = {
		.is_dpdk = false,
		.is_grpc = false,
	};
	uint32_t total_enqueued = 0;
	uint32_t total_dequeued = 0;
	long remaining_bytes;

	/* Parse cmdline/json arguments. */
	doca_argp_init("file_scan", &type_config, &app_cfg);
	register_file_scan_params();
	doca_argp_start(argc, argv, &doca_general_config);

	/* Initialize app resources and doca RegEx. */
	file_scan_init(&app_cfg);

	/* Initialize the first job request. */
	remaining_bytes = init_job_request(&app_cfg, &app_cfg.job_request);

	/* The main loop, enqueues jobs and dequeues for results. */
	do {

		total_enqueued += file_scan_enqueue_job(&app_cfg, &remaining_bytes);
		total_dequeued += file_scan_dequeue_job(&app_cfg);

	} while (remaining_bytes > 0 || total_dequeued != total_enqueued);

	DOCA_LOG_DBG("==============================");
	DOCA_LOG_DBG("File size:\t\t%ld", app_cfg.data_buffer_len);
	DOCA_LOG_DBG("Total scanned bytes:\t%ld", app_cfg.data_buffer_len - remaining_bytes);
	DOCA_LOG_DBG("Total chunks:\t\t%d", total_enqueued);
	DOCA_LOG_DBG("Total matches:\t\t%d", app_cfg.total_matches);
	DOCA_LOG_DBG("==============================");
	file_scan_cleanup(&app_cfg);

	/* ARGP cleanup */
	doca_argp_destroy();

	return 0;
}
