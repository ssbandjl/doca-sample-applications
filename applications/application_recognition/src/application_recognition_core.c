/*
 * Copyright (c) 2021-2022 NVIDIA CORPORATION & AFFILIATES, ALL RIGHTS RESERVED.
 *
 * This software product is a proprietary product of NVIDIA CORPORATION &
 * AFFILIATES (the "Company") and all right, title, and interest in and to the
 * software product, including all associated intellectual property rights, are
 * and shall remain exclusively with the Company.
 *
 * This software product is governed by the End User License Agreement
 * provided with the software product.
 *
 */

#include <signal.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <bsd/string.h>

#include <rte_sft.h>
#include <rte_malloc.h>
#include <cmdline_socket.h>
#include <cmdline_rdline.h>
#include <cmdline_parse.h>
#include <cmdline_parse_string.h>
#include <cmdline_parse_num.h>
#include <cmdline.h>

#include <doca_argp.h>
#include <doca_log.h>

#include <dpdk_utils.h>
#include <sig_db.h>
#include <utils.h>

#include "application_recognition_core.h"

DOCA_LOG_REGISTER(AR::Core);

static struct doca_dpi_ctx *dpi_ctx;

enum dpi_worker_action
set_sig_db_on_match(int queue, const struct doca_dpi_result *result, uint32_t fid, void *user_data)
{
	uint32_t sig_id = result->info.sig_id;
	struct doca_dpi_sig_data sig_data;
	struct ar_config *ar = (struct ar_config *) user_data;
	bool print_on_match = ar->print_on_match;
	bool blocked = false;
	int ret;

	ret = doca_dpi_signature_get(dpi_ctx, result->info.sig_id, &sig_data);
	if (ret != 0)
		APP_EXIT("Failed to get signatures, error=%d", ret);
	if (sig_db_sig_info_get(sig_id) == NULL)
		sig_db_sig_info_create(sig_id, sig_data.name, result->info.action == DOCA_DPI_SIG_ACTION_DROP);
	else
		sig_db_sig_info_set(sig_id, sig_data.name);
	sig_db_sig_info_fids_inc(sig_id);
	blocked = sig_db_sig_info_get_block_status(sig_id);
	if (print_on_match)
		printf_signature(dpi_ctx, sig_id, fid, blocked);
	if (blocked)
		return DPI_WORKER_DROP;
	return DPI_WORKER_ALLOW;
}

void
ar_init(const struct application_dpdk_config *dpdk_config,
	struct ar_config *ar_config, struct dpi_worker_attr *dpi_worker)
{
	int ret, err;
	uint64_t max_dpi_depth = (1 << 14); /* Max search depth. */
	struct doca_dpi_config_t doca_dpi_config = {
		/* Total number of DPI queues - set according to the number of cores */
		.nb_queues = 0,
		/* Max amount of FIDS per DPI queue */
		.max_packets_per_queue = 100000,
		/* Maximum job size in bytes for regex scan match */
		.max_sig_match_len = 5000,
	};

	/* Init signature database */
	sig_db_init();

	/* Configure regex device and queues */
	doca_dpi_config.nb_queues = dpdk_config->port_config.nb_queues;
	dpi_ctx = doca_dpi_init(&doca_dpi_config, &err);
	if (dpi_ctx == NULL)
		APP_EXIT("DPI init failed");
	if (doca_dpi_load_signatures(dpi_ctx, ar_config->cdo_filename) != 0)
		APP_EXIT("Loading DPI signature failed");

	/* Configure the attributes for the DPI worker */
	dpi_worker->dpi_on_match = set_sig_db_on_match;
	dpi_worker->user_data = (void *)ar_config;
	dpi_worker->max_dpi_depth = max_dpi_depth;
	dpi_worker->dpi_ctx = dpi_ctx;

	/* Init DOCA Telemetry netflow plugin */
	if (ar_config->netflow_source_id) {
		ret = init_netflow_schema_and_source(ar_config->netflow_source_id, "AR_netflow_metric");
		if (ret < 0)
			APP_EXIT("DOCA Telemetry Netflow init failed");
		dpi_worker->send_netflow_record = enqueue_netflow_record_to_ring;
	}
}

void
ar_destroy(struct ar_config *ar)
{
	dpi_worker_lcores_stop(dpi_ctx);

	sig_db_destroy();

	if (ar->netflow_source_id)
		destroy_netflow_schema_and_source();

	doca_dpi_destroy(dpi_ctx);
}

static void
cdo_callback(void *config, void *param)
{
	struct ar_config *ar = (struct ar_config *) config;
	char *cdo_path = (char *) param;

	if (strnlen(cdo_path, MAX_FILE_NAME) == MAX_FILE_NAME)
		APP_EXIT("CDO file name is too long - MAX=%d", MAX_FILE_NAME - 1);
	if (access(cdo_path, F_OK) == -1)
		APP_EXIT("CDO file not found %s", cdo_path);
	strlcpy(ar->cdo_filename, cdo_path, MAX_FILE_NAME);
}

static void
csv_callback(void *config, void *param)
{
	struct ar_config *ar = (struct ar_config *) config;
	char *csv_path = (char *) param;

	if (strlen(csv_path) == 0)
		return;
	if (strnlen(csv_path, MAX_FILE_NAME) == MAX_FILE_NAME)
		APP_EXIT("CSV file name is too long - MAX=%d", MAX_FILE_NAME - 1);
	strlcpy(ar->csv_filename, csv_path, MAX_FILE_NAME);
	ar->create_csv = true;
}

static void
print_match_callback(void *config, void *param)
{
	struct ar_config *ar = (struct ar_config *) config;

	ar->print_on_match = *(bool *) param;
}

static void
interactive_callback(void *config, void *param)
{
	struct ar_config *ar = (struct ar_config *) config;

	ar->interactive_mode = *(bool *) param;
}

static void
frag_callback(void *config, void *param)
{
	struct ar_config *ar = (struct ar_config *) config;

	ar->dpdk_config->sft_config.enable_frag = *(bool *) param;
}

static void
netflow_callback(void *config, void *param)
{
	struct ar_config *ar = (struct ar_config *) config;

	ar->netflow_source_id =  *(int *) param;
}

/* aux function, registration ar params to parser */
void
register_ar_params()
{
	struct doca_argp_param print_on_match_param = {
		.short_flag = "p",
		.long_flag = "print-match",
		.arguments = NULL,
		.description = "Prints FID when matched in DPI engine",
		.callback = print_match_callback,
		.arg_type = DOCA_ARGP_TYPE_BOOLEAN,
		.is_mandatory = false,
		.is_cli_only = false
	};

	struct doca_argp_param netflow_param = {
		.short_flag = "n",
		.long_flag = "netflow",
		.arguments = "<source_id>",
		.description = "Collect netflow statistics and set source_id if value is set",
		.callback = netflow_callback,
		.arg_type = DOCA_ARGP_TYPE_INT,
		.is_mandatory = false,
		.is_cli_only = false
	};
	struct doca_argp_param interactive_param = {
		.short_flag = "i",
		.long_flag = "interactive",
		.arguments = NULL,
		.description = "Adds interactive mode for blocking signatures",
		.callback = interactive_callback,
		.arg_type = DOCA_ARGP_TYPE_BOOLEAN,
		.is_mandatory = false,
		.is_cli_only = false
	};
	struct doca_argp_param csv_param = {
		.short_flag = "o",
		.long_flag = "output-csv",
		.arguments = "<path>",
		.description = "path to the output of the CSV file",
		.callback = csv_callback,
		.arg_type = DOCA_ARGP_TYPE_STRING,
		.is_mandatory = false,
		.is_cli_only = false
	};
	struct doca_argp_param cdo_param = {
		.short_flag = "c",
		.long_flag = "cdo",
		.arguments = "<path>",
		.description = "Path to CDO file compiled from a valid PDD",
		.callback = cdo_callback,
		.arg_type = DOCA_ARGP_TYPE_STRING,
		.is_mandatory = true,
		.is_cli_only = false
	};
	struct doca_argp_param frag_param = {
		.short_flag = "f",
		.long_flag = "fragmented",
		.arguments = NULL,
		.description = "Enables processing fragmented packets",
		.callback = frag_callback,
		.arg_type = DOCA_ARGP_TYPE_BOOLEAN,
		.is_mandatory = false,
		.is_cli_only = false
	};

	doca_argp_register_param(&print_on_match_param);
	doca_argp_register_param(&netflow_param);
	doca_argp_register_param(&interactive_param);
	doca_argp_register_param(&csv_param);
	doca_argp_register_param(&cdo_param);
	doca_argp_register_param(&frag_param);
	doca_argp_register_version_callback(sdk_version_callback);
}
