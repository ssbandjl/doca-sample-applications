/*
 * Copyright (c) 2021-2022 NVIDIA CORPORATION & AFFILIATES, ALL RIGHTS RESERVED.
 *
 * This software product is a proprietary product of NVIDIA CORPORATION &
 * AFFILIATES (the "Company") and all right, title, and interest in and to the
 * software product, including all associated intellectual property rights, are
 * and shall remain exclusively with the Company.
 *
 * This software product is governed by the End User License Agreement
 * provided with the software product.
 *
 */

#include <assert.h>
#include <getopt.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <doca_apsh.h>
#include <doca_argp.h>
#include <doca_log.h>
#include <doca_telemetry.h>

#include <utils.h>

#include "app_shield_agent_core.h"

DOCA_LOG_REGISTER(APSH_APP);

int
main(int argc, char **argv)
{
	struct doca_apsh_process **processes;
	struct doca_apsh_process *process = NULL;
	struct doca_apsh_attestation **attestation;
	int att_failure = 0, att_count, res;
	struct apsh_resources resources;
	struct apsh_config apsh_conf;
	struct doca_argp_program_general_config *doca_general_config;
	struct doca_argp_program_type_config type_config = {
	    .is_dpdk = false,
	    .is_grpc = false,
	};
	int runtime_file_ind;
	struct event_indexes indices;
	struct attestation_event attest_event;
	void *telemetry_schema, *telemetry_source;
	char *process_path;
	bool telemetry_enabled;

	/* Parse cmdline/json arguments */
	doca_argp_init("app_shield_agent", &type_config, &apsh_conf);
	register_apsh_params();
	doca_argp_start(argc, argv, &doca_general_config);

	/* Init the app shield agent app */
	app_shield_agent_init(&apsh_conf, &resources);

	/* Get process with 'pid' */
	process = get_process_by_pid(&processes, &resources, &apsh_conf);

	/* Creating telemetry schema */
	telemetry_enabled = !telemetry_start(&telemetry_schema, &telemetry_source, &indices);
	/* Set const values of the telemetry data */
	attest_event.pid = apsh_conf.pid;
	attest_event.scan_count = 0;
	process_path = doca_apsh_proc_info_get(process, DOCA_APSH_PROCESS_COMM);
	assert(process_path != NULL);  /* Should never happen, but will catch this error here insted of in strncpy */
	/* Copy string & pad with '\0' until MAX_PATH bytes were written. this clean the telemetry message */
	strncpy(attest_event.path, process_path, MAX_PATH);
	attest_event.path[MAX_PATH] = '\0';

	/* Get attestation */
	res = doca_apsh_attestation_get(process, apsh_conf.exec_hash_map_path, &attestation, &att_count);
	if (res) {
		doca_apsh_processes_free(processes);
		app_shield_agent_cleanup(&resources);
		APP_EXIT("attestation init failed");
	}

	/* Start attestating on loop with time_interval */
	DOCA_LOG_INFO("start attestation on pid=%d", apsh_conf.pid);
	do {
		/* Refresh attestation */
		res = doca_apsh_attst_refresh(&attestation, &att_count);
		if (res) {
			DOCA_LOG_ERR("Failed to create a new attestation, error code: %d", att_count);
			att_failure = true;
		}

		/* Check attestation */
		for (runtime_file_ind = 0; runtime_file_ind < att_count && !att_failure; runtime_file_ind++) {
			att_failure =
				  doca_apsh_attst_info_get(attestation[runtime_file_ind], DOCA_APSH_ATTESTATION_PAGES_PRESENT) !=
				  doca_apsh_attst_info_get(attestation[runtime_file_ind], DOCA_APSH_ATTESTATION_MATCHING_HASHES);
		}

		/* Send telemetry data */
		if (telemetry_enabled) {
			attest_event.timestamp = doca_telemetry_timestamp_get();
			attest_event.result = att_failure;
			if (doca_telemetry_source_report(telemetry_source, indices.attest_index, &attest_event, 1) < 0)
				DOCA_LOG_ERR("Cannot report to telemetry");
			++attest_event.scan_count;
		}

		/* Check attestation attempt status */
		if (att_failure) {
			DOCA_LOG_INFO("attestation failed");
			break;
		}
		DOCA_LOG_INFO("attestation pass");
		sleep(apsh_conf.time_interval);
	} while (true);

	/* Destroy */
	doca_apsh_attestation_free(attestation);
	doca_apsh_processes_free(processes);
	app_shield_agent_cleanup(&resources);
	if (telemetry_enabled)
		telemetry_destroy(telemetry_schema, telemetry_source);
	doca_argp_destroy();

	return 0;
}
