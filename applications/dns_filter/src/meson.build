#
# Copyright (c) 2021-2022 NVIDIA CORPORATION & AFFILIATES, ALL RIGHTS RESERVED.
#
# This software product is a proprietary product of NVIDIA CORPORATION &
# AFFILIATES (the "Company") and all right, title, and interest in and to the
# software product, including all associated intellectual property rights, are
# and shall remain exclusively with the Company.
#
# This software product is governed by the End User License Agreement
# provided with the software product.
#

app_dependencies += dependency('doca-argp')
app_dependencies += dependency('doca-flow')
app_dependencies += dependency('doca-common')
app_dependencies += dependency('doca-regex')
app_dependencies += declare_dependency(link_args : '-lresolv')

app_srcs += files([
	'dns_filter_core.c',
	common_dir_path + '/dpdk_utils.c',
	common_dir_path + '/offload_rules.c',
	common_dir_path + '/utils.c',
])

vanilla_app_srcs = [
	APP_NAME + '.c',
]

c_args = []

# Only relevant if GPU is supported
if get_option('enable_gpu_support')
	gpu_app_srcs = files([
		'dns_filter_kernel.cu',
	])
	app_srcs += gpu_app_srcs
endif

executable(DOCA_PREFIX + APP_NAME,
	app_srcs + vanilla_app_srcs,
	c_args : c_args,
	dependencies : app_dependencies,
	include_directories : app_inc_dirs,
	install: false)

# Only relevant if gRPC is supported
if get_option('enable_grpc_support')
	subdir('grpc')
endif
